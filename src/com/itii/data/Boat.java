package com.itii.data;

public abstract class Boat {
	
	private static int nbreCasesOccuppees;
	
	private boolean isPlaced;
	
	public enum BoatList {
		
		AIRCRAFT_CARRIER( "Aircraft carrier", 5 ){
			@Override
			public String toString() {
				return this.name();
			}
			
		},
		BATTLESHIP( "Battleship", 4 ){
			@Override
			public String toString() {
				return super.toString();
			}
		},
		SUBMARINE( "Submarine" , 3 ){
			@Override
			public String toString() {
				return super.toString();
			}
		},
		CRUISER( "Cruiser", 3 ){
			@Override
			public String toString() {
				return super.toString();
			}
		},
		DESTROYER( "Destroyer", 2 ){
			@Override
			public String toString() {
				return super.toString();
			}
		};
		
		public String name;
		public int nbreCases;
		
		BoatList(String name, int nbreCases){
		    this.name = name;
		    this.nbreCases = nbreCases;
		}	
		
	}
	
	public abstract BoatList getBoatType();
	
	public abstract String getName();
	
	public abstract int getLength();
	
	public abstract int getCenter();

	public static int getNbreCasesOccuppees() {
		return nbreCasesOccuppees;
	}

	public static void setNbreCasesOccuppees(int nbreCasesOccuppees) {
		Boat.nbreCasesOccuppees = nbreCasesOccuppees;
	}

	public boolean isPlaced() {
		return isPlaced;
	}

	public void setPlaced(boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

}

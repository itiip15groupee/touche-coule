package com.itii.data.boats;

import com.itii.data.Boat;
import com.itii.data.Boat.BoatList;

public class Destroyer extends Boat {
	
	private Boat.BoatList boatType;

	
	public Destroyer() {
		
		this.boatType = BoatList.DESTROYER;
		
	}


	@Override
	public BoatList getBoatType() {
		return this.boatType;
	}


	@Override
	public String getName() {
		return this.getBoatType().toString();
	}


	@Override
	public int getLength() {
		return BoatList.DESTROYER.nbreCases;
	}


	@Override
	public int getCenter() {
		return BoatList.DESTROYER.nbreCases / 2;
	}
	
}

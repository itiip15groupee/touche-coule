package com.itii.data.boats;

import com.itii.data.Boat;
import com.itii.data.Boat.BoatList;

public class Cruiser extends Boat {
	
	private Boat.BoatList boatType;

	
	public Cruiser() {
		
		this.boatType = BoatList.CRUISER;
		
	}


	@Override
	public BoatList getBoatType() {
		return this.boatType;
	}


	@Override
	public String getName() {
		return this.getBoatType().toString();
	}


	@Override
	public int getLength() {
		return BoatList.CRUISER.nbreCases;
	}


	@Override
	public int getCenter() {
		return BoatList.CRUISER.nbreCases / 2;
	}

}

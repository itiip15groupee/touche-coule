package com.itii.data.boats;

import com.itii.data.Boat;
import com.itii.data.Boat.BoatList;

public class Submarine extends Boat {
	
	private Boat.BoatList boatType;

	
	public Submarine() {
		
		this.boatType = BoatList.SUBMARINE;
		
	}


	@Override
	public BoatList getBoatType() {
		return this.boatType;
	}


	@Override
	public String getName() {
		return this.getBoatType().toString();
	}


	@Override
	public int getLength() {
		return BoatList.SUBMARINE.nbreCases;
	}


	@Override
	public int getCenter() {
		return BoatList.SUBMARINE.nbreCases / 2;
	}

}

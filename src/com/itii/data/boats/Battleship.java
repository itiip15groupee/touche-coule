package com.itii.data.boats;

import com.itii.data.Boat;
import com.itii.data.Boat.BoatList;

public class Battleship extends Boat {

private Boat.BoatList boatType;
	
	public Battleship() {
		
		this.boatType = BoatList.BATTLESHIP;
		
	}

	@Override
	public BoatList getBoatType() {
		return this.boatType;
	}

	@Override
	public String getName() {
		return this.getBoatType().toString();
	}

	@Override
	public int getLength() {
		return BoatList.BATTLESHIP.nbreCases;
	}

	@Override
	public int getCenter() {
		return BoatList.BATTLESHIP.nbreCases / 2;
	}
}

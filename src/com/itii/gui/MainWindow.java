package com.itii.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.itii.data.Boat;
import com.itii.data.boats.Battleship;
import com.itii.data.boats.Cruiser;
import com.itii.data.boats.Destroyer;
import com.itii.data.boats.Submarine;

public class MainWindow extends JFrame {
	
	private static MainWindow instance = new MainWindow();
		
	private MainWindow()
	{
		initialize();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static MainWindow getInstance()
	{
		if(instance==null)
		{
			synchronized(MainWindow.class) {
				if (instance == null) {
					instance = new MainWindow();
				}
			}
		}
		return instance;
	}
	
	public void initialize() {
		this.setSize(600, 400);
		
		Desk desk = new Desk(10);
		
		Destroyer destroyer1 = new Destroyer();
		Destroyer destroyer2 = new Destroyer();
		
		Submarine submarine1 = new Submarine();
		Submarine submarine2 = new Submarine();
		
		Cruiser cruiser = new Cruiser();
		
		Battleship battleship = new Battleship();
		
		Boat[] boatsList = {destroyer1, destroyer2, submarine1, submarine2, cruiser, battleship};
		
		
		GameMenu gameMenu = new GameMenu(boatsList);
		
		this.getContentPane().add(desk.getOpponentBoard(),BorderLayout.EAST);
		this.getContentPane().add(gameMenu,BorderLayout.CENTER);
		this.getContentPane().add(desk.getPlayerBoard(),BorderLayout.WEST);
		
		this.setVisible(true);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.validate();
		this.repaint();
	}


}

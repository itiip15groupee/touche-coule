package com.itii.gui;

import java.awt.Color;
import java.awt.Graphics;

public class SurroundingSquare extends Square{
	String text;

	public SurroundingSquare(int x, int y) {
		super(x, y);
		if(x==0)
			this.text=Character.toString ((char) (64+y));
		if(y==0)
			this.text=Integer.toString (x);
	}
	
	@Override
	public void paintSquare(Graphics g, int widthOfSquare, int heightOfSquare){
		super.paintSquare(g, widthOfSquare, heightOfSquare);
		g.setColor(Color.BLACK);
		g.fillRect(this.coordinates.getX() *widthOfSquare,
				this.coordinates.getY()*heightOfSquare,
				widthOfSquare,
				heightOfSquare);
		g.setColor(Color.WHITE);
		g.drawString(this.text, 
				this.coordinates.getX()*widthOfSquare+(int)(0.3*widthOfSquare), 
				this.coordinates.getY()*heightOfSquare+(int)(0.70*heightOfSquare));
	}

}

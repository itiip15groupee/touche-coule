package com.itii.gui;

import javax.swing.JPanel;

public class Desk extends JPanel {
	
	private GridDisplay playerBoard;
	
	private GridDisplay opponentBoard;
	
	public Desk(int nbCases) {	
		this.playerBoard = new GridDisplay(nbCases, false);
		this.opponentBoard = new GridDisplay(nbCases, true);
		
	}

	public GridDisplay getPlayerBoard() {
		return playerBoard;
	}

	public void setPlayerBoard(GridDisplay playerBoard) {
		this.playerBoard = playerBoard;
	}

	public GridDisplay getOpponentBoard() {
		return opponentBoard;
	}

	public void setOpponentBoard(GridDisplay opponentBoard) {
		this.opponentBoard = opponentBoard;
	}

}

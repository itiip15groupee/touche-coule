package com.itii.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

import com.itii.data.Coordinates;
import com.itii.data.State.StateEnum;

public class Square extends JComponent {
	
	protected Coordinates coordinates;
	
	private StateEnum mSquareState;
	
	private StateEnum temporarySquareState;
	
	public static int sizeInPixel=20;
	
	public Square (int x, int y) {
		
		this.coordinates = new Coordinates(x, y);
		mSquareState = StateEnum.EMPTY;
	}
	
	public void paintSquare(Graphics g, int widthOfSquare, int heightOfSquare){
		super.paintComponent(g);
		g.drawRect(this.coordinates.getX() *widthOfSquare,
				this.coordinates.getY()*heightOfSquare,
				widthOfSquare,
				heightOfSquare);
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}


	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	
	public final void setTemporaryState( final StateEnum temporarySquareState ) {
		this.temporarySquareState = temporarySquareState;
	}
	
	public final void freeTemporaryState() {
		temporarySquareState = null;
	}
	
	public boolean isFree() {

		boolean isFree=false;
		
		switch (getmSquareState()) {
			case BOAT:
				isFree= false;
			case EMPTY:
				isFree= true;
			case PLACING_BOAT:
				isFree = true;
			default:
				isFree = false;
		}
		
		return isFree;
	}


	public StateEnum getmSquareState() {
		return mSquareState;
	}


	public void setmSquareState(StateEnum mSquareState) {
		this.mSquareState = mSquareState;
	}


	public StateEnum getTemporarySquareState() {
		return temporarySquareState;
	}


	public void setTemporarySquareState(StateEnum temporarySquareState) {
		this.temporarySquareState = temporarySquareState;
	}

}

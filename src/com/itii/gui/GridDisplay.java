package com.itii.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import com.itii.data.Coordinates;

public class GridDisplay extends JPanel implements MouseListener, MouseMotionListener {

	private Square tab[][];
	
	private int gridSizeInPixel;

	public GridDisplay(int nbCases, boolean isOpponent) {
		
		this.tab = new Square[nbCases + 1][nbCases + 1];
		this.gridSizeInPixel = (nbCases + 1)*Square.sizeInPixel;
		
		for (int x = 0; x < this.tab.length; x++)
		{
			for (int y = 0; y < this.tab.length; y++)
			{
				if((x==0)^(y==0))
					tab[x][y]=new SurroundingSquare(x,y);
				else
					tab[x][y]= new Square(x,y);
			}
		}

	    this.setSize(gridSizeInPixel, gridSizeInPixel);
		if (isOpponent)this.setBackground(Color.RED);
		if (!isOpponent)this.setBackground(Color.GREEN);
	
	}

	public void paint(Graphics g){
	    super.paintComponent(g);
		for (int x = 0; x < this.tab.length; x++)
		{
			for (int y = 0; y < this.tab.length; y++)
			{
				tab[x][y].paintSquare(g,this.getWidth()/(this.tab.length),this.getHeight()/(this.tab.length));
			}
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
	      return this.getSize();
	   };
	   
	   
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public Square[][] getTab() {
		return tab;
	}

	public void setTab(Square tab[][]) {
		this.tab = tab;
	}

	public int getGridSizeInPixel() {
		return gridSizeInPixel;
	}

	public void setGridSizeInPixel(int gridSizeInPixel) {
		this.gridSizeInPixel = gridSizeInPixel;
	}

	public void freeAllTemporarySquareState() {
		
	}
	
}

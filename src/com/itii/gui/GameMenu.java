package com.itii.gui;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.itii.data.Boat;

public class GameMenu extends JPanel {
	
	private JComboBox<Boat> boatComboBox;
	
	private JLabel boatAvailableLabel;
	
	private JButton rotateBoatButton;
	
	private JButton readyButton;
	 
	private JButton joinButton;
	
	private JButton hitButton;
	
	private JButton surrenderButton;
	
	private JButton restartButton;

	private JButton quitButton;

	
	public GameMenu(Boat boats[]){             
	 	BoxLayout boxLayout = new BoxLayout(this,BoxLayout.Y_AXIS);
        super.setLayout(boxLayout);
	    this.setName("Menu");
	    this.setSize(100, 400);       

		boatComboBox = new JComboBox<Boat>(boats);
		
		boatAvailableLabel = new JLabel();
		rotateBoatButton = new JButton();
		readyButton = new JButton();
		joinButton = new JButton();
		hitButton = new JButton();
		surrenderButton = new JButton();
		restartButton = new JButton();
		quitButton = new JButton();
	    
	    this.add(boatComboBox);
	    this.add(boatAvailableLabel);
	    this.add(hitButton);
	    this.add(joinButton);
	    this.add(quitButton);
	    this.add(readyButton);
	    this.add(restartButton);
	    this.add(rotateBoatButton);
	    this.add(surrenderButton);

		this.setBackground(new Color(200,200,255));
	    
	    this.setVisible(true);

	  }


	public JComboBox getBoatComboBox() {
		return boatComboBox;
	}


	public void setBoatComboBox(JComboBox boatComboBox) {
		this.boatComboBox = boatComboBox;
	}


	public JLabel getBoatAvailableLabel() {
		return boatAvailableLabel;
	}


	public void setBoatAvailableLabel(JLabel boatAvailableLabel) {
		this.boatAvailableLabel = boatAvailableLabel;
	}


	public JButton getRotateBoatButton() {
		return rotateBoatButton;
	}


	public void setRotateBoatButton(JButton rotateBoatButton) {
		this.rotateBoatButton = rotateBoatButton;
	}


	public JButton getReadyButton() {
		return readyButton;
	}


	public void setReadyButton(JButton readyButton) {
		this.readyButton = readyButton;
	}


	public JButton getJoinButton() {
		return joinButton;
	}


	public void setJoinButton(JButton joinButton) {
		this.joinButton = joinButton;
	}


	public JButton getHitButton() {
		return hitButton;
	}


	public void setHitButton(JButton hitButton) {
		this.hitButton = hitButton;
	}


	public JButton getSurrenderButton() {
		return surrenderButton;
	}


	public void setSurrenderButton(JButton surrenderButton) {
		this.surrenderButton = surrenderButton;
	}


	public JButton getRestartButton() {
		return restartButton;
	}


	public void setRestartButton(JButton restartButton) {
		this.restartButton = restartButton;
	}


	public JButton getQuitButton() {
		return quitButton;
	}


	public void setQuitButton(JButton quitButton) {
		this.quitButton = quitButton;
	}      

}

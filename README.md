# Groupe E #

TP de JAVA de Mr Sébastien Martagex, création d'un touché-coulé en réseau.

### Récupérer le code ###

* Cloner ce répertoire à partir de son [URL](https://bitbucket.org/itiip15groupee/touche-coule) dans le répertoire de votre choix sur votre ordinateur :
	
```
cd $HOME/Documents
git clone https://nevermindLtd@bitbucket.org/itiip15groupee/touche-coule.git
```
* Il est fortement recommandé d'utiliser [Eclipse](http://www.eclipse.org/downloads/eclipse-packages/) pour ce projet.